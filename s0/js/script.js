 console.log("Arrays and Methods");

// let movies = ["Godfather", "Mission Impossilbe", "Avengers", "Venom", "The Nun"];

// console.log(movies[0]); // Godfather
// console.log(movies[1]); // Mission Impossible
// console.log(movies[2]); // Avengers

// movies[1] ="Halloween"


// console.log(movies[1]); // Halloween
// console.log(movies.length); //length is 5

// // We can initialize an empty array in two ways"

// // let colors = [];
// // let colors = new Array()


// //Arrays can hold any type of data

// let random_collection = [49, true, "Hermione", null];


// //Arrays have a length property

// let nums = [45, 37, 89, 50];

// console.log(nums.length);
// console.log(nums.length-1);

// //To get the last position -1 from the length

// // let colors = ["red", "orange", "yellow"];
// // console.log(colors) //Output is []  ["red", "orange", "yellow"]

// // colors.push("green");

// // -it adds in the last index of array & can push multiple 

// // console.log(colors); //Output is []  ["red", "orange", "yellow", "green"]


// // colors.pop(); // removed yellow
// // console.log(colors);


// // colors.unshift("infrared");
// // ["infrared", "red", "orange", "yellow"]
// // console.log(colors);

// //colors.shift();
// // console.log(colors);



// // Use indexOf() to find the index of an item in an array
// // let tuitt = ["Charles", "Paul", "Sef", "Alex", "Paul"];

// // //returns the first index at which a given element can be found.

// // console.log(tuitt.indexOf("Sef")); //2

// // console.log(tuitt.indexOf("Paul")); //4

// // console.log(tuitt.indexOf("Hulk")); //-1


// let colors =  ["red", "orange", "yellow", "green"];

// for(let i = 0; i < colors.length; i++){

// 	console.log(colors[i]);
// } // for loop statement.


// colors.forEach(function(color){

// //color is a placeholder
// console.log(color);


// });

// for( let i = 0; i < colors.length; i++)
// {
// 	console.log(`${colors[i]} is in ${i}`);

// 	//template literals
// }

// colors.forEach(function(color){

// console.log(`${color} is in  ${colors.indexOf(color)}`)


// }); //Written in for and forEach Methods

// const colors = ['red', 'green', 'blue'];
// //array reside in the memory address 

// let colors2 = ['red', 'green', 'blue'];


// let colors3 = colors;

// const scores = [
  
//   [12, 15, 16],
//   [11, 9, 8],
//   [1, 5, 6],


// ];

// console.log(scores[1][2]);
// //to call multideminsional array started with the col and row


// scores.forEach(function(score){

// console.log(score)

// });

// ///for loop design

// for(let i = 0; i < scores.length; i++){

// 		console.log(scores[i])
	
// }; // for loop each element list 


// //per array list in multi dimensional

// for(let i = 0; i < scores.length; i++){

// 	for(let j = 0; j < scores[i].length; j++){

// 		console.log(scores[i][j])
// 	}

// }; // for loop each element list 


console.log('**********Objects**********');

const grades = [98, 87, 91, 84];
// arrays has no labels


// const myGrades = {

// //properties
//  lastName: 'Zamora',
//  firstName: 'Rie',
//  science: 98,
//  match: 87,
//  english: 91,
//  programming: 94,
//  hobbies: ['coding', 'writing', 'cosplay'] // array use

// }; //Objects use label for elements of an array

//console.log(myGrades.science);
//Key value pairs


const person = {

 firstName: 'John',
 lastName:  'Smith',
 emails: ['jsmith@gmail.com', 'js@gmail.com'],
 address: {
   //additional properties inside
 		city: 'Tokyo',
 		country: 'Japan'
 },
 //additional it is call now method since inside the object
 greeting: function(){

 	return 'hi';
 }
};

//an array of an object for e-commerce app
const shoppingCart = [
 {
 	product: 'shirt',
 	price: 99.90,
 	qtt: 2

 },
 {
 	product: 'watch',
 	price: 9.90,
 	qtt: 1


 },
 {

 	product: 'laptop',
 	price: 999.90,
 	qtt: 2

 },
];

// const blogger = {

// 	lname: 'Smith',
// 	fname: 'John',
// 	post: [

// 		{
// 			title: 'My first post',
// 			body: 'Lorem Ipsum',
// 			comments: [ 

// 				{}

// 			]
// 		},
// 		{

// 		},
// 		{

// 		}

// 	]
// }